#include <set>
#include <map>
#include <algorithm>
#include <vector>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include <deque>

#include "mongoose.h"


using namespace std;


/* Global */


struct mg_mgr mongoose_manager;

set <string> peers {
	string {"https://platyca.tk/"},
	string {"https://register.platyca.tk/"},
};


/* Util */


static string make_string (struct mg_str in)
{
	return string {in.ptr, in.len};
}


static bool starts_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (0, y.size ()) == y;
}


static bool ends_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (x.size () - y.size ()) == y;
}


/* Server */


static bool by_length (string left, string right)
{
	return left.size () < right.size ();
}


static string escape_csv (string in)
{
	string out;
	for (auto c: in) {
		if (c == '"') {
			out += string {"\"\""};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


static string construct_peers_page () {
	string csv;
	
	vector <string> peers_vector {peers.begin (), peers.end ()};
	stable_sort (peers_vector.begin (), peers_vector.end (), by_length);

	for (string peer: peers_vector) {
		csv += string {"\"peer\",\""} + escape_csv (peer) + string {"\"\r\n"};
	}

	return csv;
};


static string construct_404_page ()
{
	string html = string {
		"<!DOCTYPE html>\r\n"
		"<meta name=\"viewport\" content=\"width=device-width\">\r\n"
		"<title>404</title>\r\n"
		"<p>404</p>\r\n"
	};

	return html;
}


static bool valid (string peer)
{
	return
		(
			starts_with (peer, string {"http://"})
			|| starts_with (peer, string {"https://"})
		) && (
			ends_with (peer, "/")
		);
}


static void cb (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	if (ev == MG_EV_HTTP_MSG) {
		auto http_message = reinterpret_cast <struct mg_http_message *> (ev_data);
		if (make_string (http_message->method) == string {"GET"}) {
			if (make_string (http_message->uri) == string {"/"}) {
				mg_http_serve_file (
					c,
					http_message,
					"registerer.html",
					"text/html",
					nullptr
				);
			} else if (make_string (http_message->uri) == string {"/peers.csv"}) {
				mg_http_reply (
					c,
					200,
					"Content-Type: text/plain\r\n",
					"%s",
					construct_peers_page ().c_str ()
				);
			} else {
				mg_http_reply (
					c,
					404,
					"Content-Type: text/html\r\n",
					"%s",
					construct_404_page ().c_str ()
				);
			}
		} else if (make_string (http_message->method) == string {"POST"}) {
			if (make_string (http_message->uri) == string {"/"}) {
				string payload = make_string (http_message->body);
				if (valid (payload)) {
					peers.insert (payload);
					mg_http_reply (
						c,
						200,
						"Content-Type: text/plain\r\n",
						"OK"
					);
				} else {
					mg_http_reply (
						c,
						500,
						"Content-Type: text/plain\r\n",
						"Invalid peer."
					);
				}
			}
		}
	}
}


/* Main */


int main (int argc, char *argv []) {
	mg_mgr_init (& mongoose_manager);
	mg_http_listen (& mongoose_manager, "http://localhost:8001", cb, nullptr);
	for (; ; ) {
		mg_mgr_poll (& mongoose_manager, 1000);
	}
	mg_mgr_free (& mongoose_manager);
	return 0;
}

